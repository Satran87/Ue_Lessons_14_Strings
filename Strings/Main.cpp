#include <cstdlib>
#include <string>
#include <iostream>
using namespace std;
int main()
{
	string myString("SomeText");
	cout << "Base string = " << myString << endl;
	cout << "Base string size = " << myString.size() << endl;
	try
	{
		cout << "First symbol = " << myString.at(0) << endl;
		cout << "Last symbol = " << myString.at(myString.size() - 1) << endl;
	}
	catch (const std::out_of_range & oor)
	{
		cout << "Out of Range error: " << oor.what() << endl;
	}
	system("pause");
	return EXIT_SUCCESS;
}